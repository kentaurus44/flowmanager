﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

[CustomEditor(typeof(FlowDatabase))]
public class FlowDatabaseEditor : Editor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
		var database = (FlowDatabase)target;
		if (GUILayout.Button("Load"))
		{
			string[] files = Directory.GetFiles(string.Concat(Application.dataPath, "/ScriptableObjects/Configs/Flow/"), "*.asset", SearchOption.AllDirectories);

			if (files.Length > 0)
			{
				database.Configs.Clear();
				foreach (string file in files)
				{
					string assetPath = "Assets" + file.Replace(Application.dataPath, "").Replace('\\', '/');
					FlowConfig item = AssetDatabase.LoadAssetAtPath<FlowConfig>(assetPath);
					database.Configs.Add(item);
				}
			}
		}
	}
}
