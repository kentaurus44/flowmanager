﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Core.Flow
{
	public static class FlowUtilities
	{
		[MenuItem("Assets/Create Flow Config")]
		private static void NewMenuOptionValidation()
		{
			FlowConfig config = ScriptableObject.CreateInstance<FlowConfig>();
			string name = Selection.activeObject.name;
			AssetDatabase.CreateAsset(config, string.Format("Assets/Config/Flow/{0}.asset", name));
			AssetDatabase.SaveAssets();

			FlowDatabase flowDatabase = FlowDatabase.GetEditorInstance();

			flowDatabase.AddFlowConfig(config);

			Debug.LogFormat("Editor/Flow", "{0} created.", name);

		}
	}
}