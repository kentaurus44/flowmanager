﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class DatabaseManager
{
	[SerializeField]
	protected FlowDatabase _flowDatabase;

	public FlowDatabase FlowDatabase { get { return _flowDatabase; } }
}