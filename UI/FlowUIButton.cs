﻿using UnityEngine;
using Core.Flow;

public class FlowUIButton : UIButton
{
	[SerializeField, FlowActionAttribute]
	protected string _action;
	[SerializeField, FlowAttribute]
	protected string _view;

	protected override void OnButtonCB()
	{
		base.OnButtonCB();
		FlowManager.Instance.TriggerAction(_action, _view);
	}
}
